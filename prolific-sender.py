#9.4 Write a program to read through the mbox-short.txt and figure out who has the sent the greatest number of mail messages. The program looks for 'From ' lines and takes the second word of those lines as the person who sent the mail. The program creates a Python dictionary that maps the sender's mail address to a count of the number of times they appear in the file. After the dictionary is produced, the program reads through the dictionary using a maximum loop to find the most prolific committer.


name = raw_input("Enter file:")
if len(name) < 1 : name = "mbox-short.txt"
handle = open(name)

dictionary = dict()

for line in handle :
	if (line.startswith( 'From ') )  :	
		lista = line.rstrip().split()[1]
		#print lista
		
		if lista not in dictionary :
			dictionary[lista] = 1
			#print "asi va", dictionary
		else :
			dictionary[lista] = dictionary[lista] + 1

#print dictionary

#elementos = len(dictionary)

#print dictionary.keys()
#print dictionary.values()
repeticion = dictionary.values()
correo = dictionary.keys()

valor = max(dictionary.values()) #max value in list
posicion = repeticion.index(valor) #find the position of the max value
print correo[posicion] + " " + str(valor)

